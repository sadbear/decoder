#ifndef DECODER_CONTEXT_HPP
#define DECODER_CONTEXT_HPP

#include "Attribute.hpp"
#include "../Service/hash_combine.hpp"

#include <map>
#include <string>
#include <numeric>
#include <variant>
#include <functional>
#include <string_view>

namespace specification
{
    using Context = std::map<Attribute, std::variant<int, std::string>>;
    using CheckContextFunc = std::function<std::variant<int, std::string>(const Attribute &)>;
}

namespace std
{
	template <>
	class hash<specification::Context>
	{
	public:
		size_t operator()(const specification::Context & context)
		{
			hash<specification::Attribute> hash_attribute;
			hash<variant<int, string>> hash_variant;

			std::vector<size_t> hash_values;
			for (const auto & [attribute, value]: context)
			{
				hash_values.push_back(
						hash_combine(hash_attribute(attribute), hash_variant(std::get<int>(value)))
						);
			}

			const auto value = accumulate(
					std::next(hash_values.begin()), hash_values.end(), hash_values.front(),
					[](size_t acc, size_t hash)
					{
						return hash_combine(acc, hash);
					});

			return value;
		}
	};
}

#endif //DECODER_CONTEXT_HPP
