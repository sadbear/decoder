#ifndef DECODER_ITEMPLATE_H
#define DECODER_ITEMPLATE_H

#include <set>
#include <list>
#include <memory>
#include <vector>
#include <variant>

#include "Indexes.hpp"
#include "Signature.hpp"
#include "Context.hpp"

namespace specification
{
    struct ITemplate
    {
        using Ptr = std::shared_ptr<ITemplate>;

        virtual ~ITemplate() = default;

	    virtual size_t size() const = 0;

        virtual std::variant<Indexes, bool> check(const specification::Signature &signature) const = 0;

        virtual Indexes getIndexes() const = 0;

	    virtual Signature getSignature() const = 0;

	    virtual bool isEqual(const Ptr &other) = 0;

        virtual bool isIntersect(const Ptr &other) = 0;

        /// Difference with other template function
        /// \param other - other template pointer of inherited type
        /// \return Return `this->empty()` after operation
        virtual bool operator-=(const Indexes &other) = 0;
    };

    using TemplatePtr = ITemplate::Ptr;
    using TemplateCompare = std::function<bool(const specification::TemplatePtr &, const specification::TemplatePtr &)>;
}

#endif //DECODER_ITEMPLATE_H
