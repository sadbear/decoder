#ifndef DECODER_SIGNATURE_HPP
#define DECODER_SIGNATURE_HPP

#include <vector>

#include "../Service/hash_combine.hpp"
#include "Indexes.hpp"

namespace specification
{
    class Signature
    {
    public:
	    Signature();
        Signature(const Indexes & indexes, const std::vector<bool> & signature);
        Signature(std::initializer_list<std::pair<uint, bool>>);

	    Signature(const Indexes &indexes, uint64_t value);

        Signature operator[](const Indexes & indexes) const;
        bool      operator[](uint index) const;

        Indexes getIndexes() const;

        std::vector<Signature> getContinuesParts() const;

        uint64_t toInt() const;
        std::vector<bool> getRaw() const;

	    bool   empty() const;
        size_t size() const;

        bool      operator ==(const Signature & other) const;
        Signature operator^(const Signature& other) const;
	    Signature operator^(const Indexes & other) const;
        Signature operator+(const Signature & other) const;
		Signature operator-(const Indexes &other) const;

    private:
        Indexes           _indexes;
        std::vector<bool> _signature;
    };
}

namespace std
{
	template <>
	class hash<specification::Signature>
	{
	public:
		size_t operator()(const specification::Signature & signature)
		{
			size_t hash;
			{
				std::hash<specification::Indexes> hash_fn;
				hash = hash_fn(signature.getIndexes());
			}
			{
				std::hash<std::vector<bool>> hash_fn;
				hash = hash_combine(hash, hash_fn(signature.getRaw()));
			}

			return hash;
		}
	};
}

#endif //DECODER_SIGNATURE_HPP
