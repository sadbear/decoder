#ifndef DECODER_OPERAND_H
#define DECODER_OPERAND_H

#include <map>
#include "ITemplate.hpp"

namespace specification
{
    struct OperandPart
    {
	public:
    	using Content = std::variant<Indexes, Signature>;

    public:
		Indexes getIndexes() const;
		std::optional<Signature> getSignature() const;

		bool isHaveSignature() const;

	public:
		Attribute attribute;
		Content   part;

	};

    class Operand: public ITemplate
    {
    public:
    	using Ptr = std::shared_ptr<Operand>;
    	
    public:
		explicit Operand(Attribute addressing_mode, std::vector<std::pair<Attribute, OperandPart::Content>> parts);
        Operand(Attribute addressing_mode, std::initializer_list<std::pair<Attribute, OperandPart::Content>> parts);

	    size_t size() const override;

        std::variant<Indexes, bool> check(const specification::Signature &signature) const override;

        Indexes getIndexes() const override;
        Signature getSignature() const override;
        
		bool isHaveConstant() const;
        bool isEqual(const ITemplate::Ptr &other) override;

        bool isIntersect(const ITemplate::Ptr &other) override;

        bool operator-=(const Indexes &other) override;

        inline bool operator<(const Operand & rhs) const
		{
        	return size() < rhs.size();
		}

        const std::vector<OperandPart> & getParts() const;
        
    private:
        std::vector<OperandPart> _operandTable;
        Attribute _addressing_mode;
    };
}

namespace std
{
	template <>
	class hash<specification::OperandPart>
	{
	public:
		size_t operator()(const specification::OperandPart & part)
		{
			hash<specification::Attribute> hash_attribute;
			hash<specification::OperandPart::Content> hash_part;
			return hash_combine(hash_attribute(part.attribute), hash_part(part.part));
		}
	};
	
	template <>
	class hash<specification::Operand>
	{
	public:
		size_t operator()(const specification::Operand& operand)
		{
			hash<specification::OperandPart> hash_part;
			
			if (operand.getParts().empty())
				return 0;
			
			return std::accumulate(std::next(operand.getParts().begin()), operand.getParts().end(), hash_part(operand.getParts()[0]),
						[&hash_part](auto accumulator, const auto & part)
						{
							return hash_combine(accumulator, hash_part(part));
						});
		}
	};
}
#endif //DECODER_OPERAND_H
