#ifndef DECODER_INSTRUCTION_HPP
#define DECODER_INSTRUCTION_HPP

#include "ITemplate.hpp"
#include "OperandCollection.hpp"

namespace specification
{
    struct Instruction
    {
    public:
        Instruction(const TemplateSet & templates, const Context & context);

        size_t weight() const;

        bool operator<(const Instruction & instruction) const;

        std::string getMnemonic() const;
        Signature getSignature() const;
        Indexes getIndexes() const;

        Signature getNormalizeSignature() const;

    public:
	    static void compressOperands(std::set<TemplatePtr, TemplateCompare> &templates);

    public:
        const specification::TemplateSet _templates;
        const Context                    _context;
    };
}

#endif //DECODER_INSTRUCTION_HPP
