#ifndef DECODER_INDEXES_HPP
#define DECODER_INDEXES_HPP

#include "../Service/hash_combine.hpp"

#include <set>
#include <vector>
#include <numeric>
#include <optional>

namespace specification
{
    class Indexes : public std::set<uint>
    {
    public:
        static Indexes getRange(uint first, uint end);

    public:
        Indexes();
        Indexes(std::set<uint>::iterator begin, std::set<uint>::iterator end);

        Indexes(std::initializer_list<uint> indexes);

        bool                isSeparated() const;
        std::optional<bool> isConsistent(const Indexes & other);
		std::vector<Indexes> getContinuesParts() const;
		
		bool contain(const Indexes & indexes) const;
		
        unsigned int back() const;
        unsigned int front() const;

        void shrinkToContinuous();

        Indexes operator+(const Indexes & other) const;
	    Indexes operator+(uint other) const;
        Indexes operator^(const Indexes & other) const;
        Indexes operator-(const Indexes & other) const;

        void    operator+=(const Indexes & other);
        void    operator+=(uint index);
        Indexes operator^=(const Indexes & indexes);

        std::optional<bool> operator<(const Indexes & right) const;
        std::optional<bool> operator>(const Indexes & other) const;
    };
}

namespace std
{
    template <>
    class hash<specification::Indexes>
    {
    public:
        size_t operator()(const specification::Indexes & indexes)
        {
            hash<uint> hash_fn;
		
	        return std::accumulate(std::next(indexes.begin()), indexes.end(),
							indexes.front(),
							[](size_t hash, uint indexes)
							{
                                return hash_combine(hash, indexes);
							});
        }
    };
}

#endif //DECODER_INDEXES_HPP

