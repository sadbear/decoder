#include "../Indexes.hpp"
#include "../Morpheme.hpp"

namespace specification
{
	
	size_t Morpheme::size() const
	{
		return _constant.size();
	}

	std::variant<Indexes, bool> Morpheme::check(const Signature &signature) const
	{
		if (_constant == signature)
			return {true};

		if (signature.size() > _constant.size())
			return {false};

		auto intersect = signature ^_constant;

		if (intersect.empty())
			return {false};

		return {intersect.getIndexes()};
	}

	Indexes Morpheme::getIndexes() const
	{
		return _constant.getIndexes();
	}

	bool Morpheme::isEqual(const ITemplate::Ptr &other)
	{
		throw std::logic_error("Not implemented.");
	}

	bool Morpheme::isConstant() const
	{
		return _mnemonic == std::nullopt;
	}

	std::optional<Morpheme::Position> Morpheme::getPosition() const
	{
		return _position;
	}

	bool Morpheme::isIntersect(const ITemplate::Ptr &other)
	{
		throw std::logic_error("Not implemented.");
	}

	Morpheme::Morpheme(const Signature &signature,
	                   std::string_view morpheme,
	                   const Morpheme::Position position)
			: _constant(signature), _mnemonic(morpheme), _position(position)
	{}

	bool Morpheme::operator-=(const Indexes &other)
	{
		_constant = _constant - other;
		return true;
	}

	Morpheme::Morpheme(const Signature & signature): _constant(signature)
	{}

	Signature Morpheme::operator^(ITemplate::Ptr other) const
	{
		return _constant ^ other->getSignature();
	}

	void Morpheme::operator^=(const Indexes &other)
	{
		_constant = _constant ^ other;
	}

	Signature Morpheme::getSignature() const
	{
		return _constant;
	}

	std::optional<std::string> Morpheme::toString() const
	{
		return _mnemonic;
	}

	Morpheme Morpheme::operator+(const Morpheme &other) const
	{
		const auto signature = getSignature() + other.getSignature();

		auto mnemonic = _mnemonic, other_mnemonic = other._mnemonic;
		if (mnemonic && other_mnemonic)
			throw std::logic_error("I can't sum two morphemes with mnemonics");

		if (mnemonic)
			return Morpheme(signature, *mnemonic, *getPosition());
		if (other_mnemonic)
			return Morpheme(signature, *other_mnemonic, *other.getPosition());

		return Morpheme(signature);

	}

	Morpheme Morpheme::operator+(const Ptr &other) const
	{
		return this->operator+(*other);
	}

	Morpheme::Ptr operator+(const Morpheme::Ptr &left, const Morpheme::Ptr &right)
	{
		return std::make_shared<Morpheme>(left->operator+(right));
	}
}

