#include <vector>
#include <utility>
#include <iterator>
#include <algorithm>
#include <assert.h>

#include "../Indexes.hpp"

specification::Indexes specification::Indexes::operator+(const specification::Indexes &other) const
{
    std::set<unsigned int> indexes = other;
    indexes.insert(begin(), end());

    return Indexes(indexes.begin(), indexes.end());
}

std::optional<bool> specification::Indexes::operator<(const specification::Indexes &right) const
{
    if (!isSeparated() && !right.isSeparated())
    {
    	const auto leftStart = front(), leftBack = back(), rightStart = right.front(), rightBack = right.back();

        if (leftBack < rightStart)
            return true;

        if (rightBack < leftStart)
            return false;

        return {};
    }

    bool result = true;

    for (const auto index : *this)
    {
        for (const auto &other_index: right)
        {
            if (index == other_index)
                return {};
            if (index > other_index)
                result = false;
        }
    }
    return result;
}

std::optional<bool> specification::Indexes::operator>(const specification::Indexes &other) const
{
    auto result = *this < other;
    if (result)
        return !*result;
    return result;
}

bool specification::Indexes::isSeparated() const
{
    if (empty())
        return false;
    return (back() - front()) != (std::distance(cbegin(), cend()) - 1);
}

unsigned int specification::Indexes::back() const
{
    return *(--end());
}

unsigned int specification::Indexes::front() const
{
    return *begin();
}

void specification::Indexes::operator+=(const specification::Indexes &other)
{
    this->insert(other.begin(), other.end());
}

specification::Indexes specification::Indexes::operator^(const specification::Indexes &other) const
{
    Indexes indexes{};
    std::set_intersection(
            begin(), end(),
            other.begin(), other.end(),
            std::inserter(indexes, indexes.begin()));
    return Indexes(indexes);
}


std::optional<bool> specification::Indexes::isConsistent(const specification::Indexes &other)
{
    if (isSeparated() || other.isSeparated())
        return {};
    std::optional<bool> isBiggerResult = *this > other;
    if (!isBiggerResult)
        return {};
    if (*isBiggerResult)
        return false;
    return this->back() + 1 == other.front();
}

specification::Indexes::Indexes(std::initializer_list<uint> indexes) : std::set<uint>(indexes)
{
    if (size() != indexes.size())
        throw std::out_of_range("Indexes initializer list has repeating indexes.");
}

void specification::Indexes::operator+=(uint other)
{
    insert(other);
}

specification::Indexes specification::Indexes::getRange(uint first, uint end)
{
    assert(first < end);

    Indexes indexes{};
    for (auto index = first; index != end; ++index)
        indexes.insert(index);
    return indexes;
}

void specification::Indexes::shrinkToContinuous()
{
    if (!isSeparated())
        return;
    auto start = front();
    std::set<uint>::iterator it;
    for (it = begin(); it != end(); ++it)
        if (*it != start++)
            break;
    erase(it, end());
}

specification::Indexes::Indexes(std::set<uint>::iterator begin, std::set<uint>::iterator end)
        : std::set<uint>(begin, end)
{
}

specification::Indexes::Indexes() = default;

specification::Indexes specification::Indexes::operator-(const specification::Indexes &other) const
{
    Indexes indexes;
    std::set_difference(
            begin(), end(),
            other.begin(), other.end(),
            std::inserter(indexes, indexes.begin()));
    return indexes;
}

specification::Indexes specification::Indexes::operator^=(const specification::Indexes &indexes)
{
	throw std::logic_error("Not implemented.");
}

std::vector<specification::Indexes> specification::Indexes::getContinuesParts() const
{
    if (!isSeparated())
        return {Indexes(*this),};

    std::vector<Indexes> result;
    Indexes current;
    for (unsigned int index : *this)
    {
        if ((current + index).isSeparated())
        {
            result.push_back(current);
            current.clear();
            current.insert(index);
        }
        else
        {
            current.insert(index);
        }
    }

    if (!current.empty())
        result.push_back(current);

    return result;
}

specification::Indexes specification::Indexes::operator+(uint other) const
{
    Indexes result(*this);
    result.insert(other);
    return result;
}

bool specification::Indexes::contain(const Indexes & other) const
{
    return std::includes(begin(), end(), other.begin(), other.end());
}
