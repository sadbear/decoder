#include <numeric>

#include "../Operand.hpp"


size_t specification::Operand::size() const
{
	return getIndexes().size();
}

std::variant<specification::Indexes, bool> specification::Operand::check(const specification::Signature &signature) const
{
	const auto operandSignature = getSignature();
	const auto operandIndexes = getIndexes();
	
	if (!signature.getIndexes().contain(operandIndexes))
		return signature.getIndexes() ^ operandIndexes;
	
	if (operandSignature.empty())
		return true;
	
	const auto intersect = operandSignature ^ signature;
	
	if (operandSignature == intersect)
		return true;
	
	auto result = (operandSignature ^ signature).getIndexes();
	
	return result.empty()
					? std::variant<Indexes, bool>{false}
					: std::variant<Indexes, bool>{result};
}

specification::Indexes specification::Operand::getIndexes() const
{
	return std::accumulate(_operandTable.begin(), _operandTable.end(), Indexes{},
	                       [](Indexes &sum, const OperandPart &part)
	                       {
		                       return sum + part.getIndexes();
	                       });
}

bool specification::Operand::isEqual(const specification::ITemplate::Ptr &other_)
{
//	if (auto other = std::dynamic_pointer_cast<Operand>(other_); other)
//		return _operandTable == other->_operandTable;
//
	return false;
}

bool specification::Operand::isIntersect(const specification::ITemplate::Ptr & other)
{
	if (isHaveConstant())
		return !(getSignature() ^ other->getSignature()).empty();
	else
		return !(getIndexes() ^ other->getIndexes()).empty();
}

bool specification::Operand::operator-=(const Indexes &other)
{
	throw std::logic_error("Not implemented"); // ToDo
}

specification::Signature specification::Operand::getSignature() const
{
	return std::accumulate(_operandTable.begin(), _operandTable.end(), Signature{},
	                       [](Signature &sum, const OperandPart &part)
	                       {
		                       if (part.isHaveSignature())
			                       return sum + *part.getSignature();
		                       return sum;
	                       });
}

bool specification::Operand::isHaveConstant() const
{
	return std::any_of(_operandTable.begin(), _operandTable.end(), [](const OperandPart & part) { return part.isHaveSignature();});
}

specification::Operand::Operand(Attribute addressing_mode, std::vector<std::pair<Attribute, OperandPart::Content>> parts):
_addressing_mode(addressing_mode)
{
	std::transform(parts.begin(), parts.end(), std::back_inserter(_operandTable),
			[](const auto & pair)
			{
				return OperandPart{pair.first, pair.second};
			});
}

specification::Operand::Operand(Attribute addressing_mode, std::initializer_list<std::pair<Attribute, OperandPart::Content>> parts):
_addressing_mode(addressing_mode)
{
	std::transform(parts.begin(), parts.end(), std::back_inserter(_operandTable),
	               [](const auto & pair)
	               {
		               return OperandPart{pair.first, pair.second};
	               });
}

const std::vector<specification::OperandPart> & specification::Operand::getParts() const
{
	return _operandTable;
}

bool specification::OperandPart::isHaveSignature() const
{
	return getSignature() != std::nullopt;
}

specification::Indexes specification::OperandPart::getIndexes() const
{
	try
	{
		return std::get<Indexes>(part);
	}
	catch (std::bad_variant_access&)
	{
		return std::get<Signature>(part).getIndexes();
	}
}

std::optional<specification::Signature> specification::OperandPart::getSignature() const
{
	try
	{
		return std::get<Signature>(part);
	}
	catch(std::bad_variant_access&)
	{
		return std::nullopt;
	}
}


