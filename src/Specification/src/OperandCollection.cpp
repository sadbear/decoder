#include <numeric>
#include "../OperandCollection.hpp"
#include "../Operand.hpp"

specification::OperandCollection::OperandCollection() = default;


size_t specification::OperandCollection::operandsSize() const
{
	return static_cast<size_t>(std::accumulate(begin(), end(), 0,
								   [](auto size, const auto &op) -> size_t { return size + op.size(); }));
}

std::variant<specification::Indexes, bool> specification::OperandCollection::check(const specification::Signature &signature) const
{
	if (empty())
		return false;

	Indexes result;
	for (auto & operand: *this)
	{
		const auto check = operand.check(signature);
		try
		{
			if (!std::get<bool>(check))
				return false;
		}
		catch (std::bad_variant_access&)
		{
			result ^= std::get<Indexes>(check);
		}
	}

	if (result.empty())
		return true;

	return result;
}

specification::Indexes specification::OperandCollection::getIndexes() const
{
	return std::accumulate(begin(), end(), Indexes(),
	                       [](auto indexes, const auto &op) { return indexes + op.getIndexes(); });
}

bool specification::OperandCollection::isEqual(const specification::ITemplate::Ptr &other)
{
//	if (auto collection = std::dynamic_pointer_cast<OperandCollection>(other); collection)
//		return *this == collection;
//	if (auto operand = std::dynamic_pointer_cast<Operand>(other); operand)
//		return _operands.size() == 1 && _operands.find(operand) != _operands.end();

	return false;
}

bool specification::OperandCollection::isIntersect(const specification::ITemplate::Ptr &other)
{
	if (auto collection = std::dynamic_pointer_cast<OperandCollection>(other); collection)
	{
		return std::any_of(begin(), end(),
				[collection](const auto operand) { return collection->find(operand) != collection->end(); });
	}

	if (auto operand = std::dynamic_pointer_cast<Operand>(other); operand)
	{
		return find(*operand) != end();
	}

	return false;
}

bool specification::OperandCollection::operator-=(const Indexes &other)
{
	throw std::logic_error("Not implemented");
}

specification::Signature specification::OperandCollection::getSignature() const
{
	return std::accumulate(begin(), end(), Signature(),
	                       [](auto accum, const auto &operand)
	                       { return accum + operand.getSignature(); });
}

size_t specification::OperandCollection::getWeight() const
{
	return std::accumulate(begin(), end(), (size_t)0,
			[](size_t weight, const auto & operand) { return weight + operand.size(); });
}

specification::OperandCollection::Ptr specification::OperandCollection::create()
{
	return std::make_shared<OperandCollection>();
}

size_t specification::OperandCollection::size() const
{
	auto ptr = (std::set<Operand::Ptr>*)this;
	return ptr->size();
}

template < class Iterator, class U >
specification::OperandCollection::Ptr specification::OperandCollection::create(Iterator begin, Iterator end)
{
    auto collection = OperandCollection::create();
    std::for_each(begin, end, [&collection](const auto & val){collection->insert(val);});
	return collection;
}

