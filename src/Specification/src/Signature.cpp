#include <list>
#include <bitset>
#include <algorithm>

#include "../Signature.hpp"

specification::Signature::Signature(const specification::Indexes &indexes, const std::vector<bool> &signature)
: _indexes(indexes), _signature(signature)
{
    if (indexes.size() != signature.size())
        throw std::out_of_range("Size of indexes != getWeight of signature");
}

specification::Signature specification::Signature::operator[](const specification::Indexes & indexes) const
{
	const auto size = indexes.size();
	const auto intersectedIndexes = indexes ^ _indexes;

	if (intersectedIndexes.size() < size)
        throw std::out_of_range("Indexes argument is't subset of intercsectedIndexes `Signature` instance.");

    std::vector<bool> result;
    result.reserve(intersectedIndexes.size());

    auto ptr = _indexes.begin();
    for (const auto & index: intersectedIndexes)
    {
        ptr = std::find(ptr, _indexes.end(), index);
        result.push_back(*(_signature.begin() + std::distance(_indexes.begin(), ptr)));
    }

    return Signature{intersectedIndexes, result};
}

bool specification::Signature::operator[](uint index) const
{
    auto it = std::find(_indexes.begin(), _indexes.end(), index);
    if (it == _indexes.end())
        throw std::out_of_range("Index not in range.");

    return *(_signature.begin() + std::distance(_indexes.begin(), it));
}

size_t specification::Signature::size() const
{
    return _signature.size();
}

bool specification::Signature::operator==(const specification::Signature &other) const
{
    return _indexes == other._indexes && _signature == other._signature;
}

specification::Signature specification::Signature::operator^(const specification::Signature &other) const
{
    Indexes indexes = (_indexes ^ other._indexes);

	Signature result;

	for (const auto &index: indexes)
	{
		const auto bit = this->operator[](index);
		const auto otherBit = other[index];
		if (bit == otherBit)
		{
			result._indexes.insert(index);
			result._signature.push_back(bit);
		}
	}

	return result;
}

specification::Indexes specification::Signature::getIndexes() const
{
    return _indexes;
}

specification::Signature::Signature(std::initializer_list<std::pair<uint, bool>> signature_map)
        : _indexes{}
{
    _signature.reserve(signature_map.size());

    for (const auto [index, value]: signature_map)
    {
        _indexes += index;
        _signature.push_back(value);
    }
}

specification::Signature::Signature(const specification::Indexes &indexes, uint64_t value)
: _indexes(indexes)
{
	_signature.reserve(indexes.size());
	uint begin = indexes.front();
	for (const auto index: _indexes)
	{
		const auto bit = bool((value >> (index - begin)) & 1);
		_signature.push_back(bit);
	}
}

bool specification::Signature::empty() const
{
    return _indexes.empty();
}

specification::Signature::Signature() = default;

specification::Signature specification::Signature::operator-(const Indexes &other) const
{
	const auto indexes = _indexes - other;

    std::vector<bool> signature;
    std::transform(indexes.begin(), indexes.end(), std::back_inserter(signature), [this](const auto index)
    {
        return operator[](index);
    });

    return Signature(indexes, signature);
}

specification::Signature specification::Signature::operator+(const specification::Signature &other) const
{
	if (!(getIndexes() ^ other.getIndexes()).empty())
		throw std::logic_error("Can't sum intersected signature");

	auto indexes = _indexes + other._indexes;
	std::vector<bool> signature;

	for (const auto index: indexes)
	{
		if (_indexes.find(index) != _indexes.end())
			signature.push_back(operator[](index));
		else if (other._indexes.find(index) != other._indexes.end())
			signature.push_back(other[index]);
		else
			throw std::logic_error("Error during summation of indexes within sum of signatures.");
	}

    return Signature(indexes, signature);
}

uint64_t specification::Signature::toInt() const
{
	uint64_t value = 0x0;

	for (auto index: _indexes)
	{
		const auto bit = -(unsigned long) this->operator[](index);
		index -= _indexes.front();
		value ^= (bit ^ value) & (1UL << index);
	}

	return value;
}

specification::Signature specification::Signature::operator^(const specification::Indexes &other) const
{
	auto indexes = _indexes ^ other;

	std::vector<bool> constant;
	constant.reserve(indexes.size());

	std::transform(indexes.begin(), indexes.end(), std::back_inserter(constant), [this](const auto index)
	{
		return this->operator[](index);
	});

	return Signature{indexes, constant};
}

std::vector<bool> specification::Signature::getRaw() const
{
	return _signature;
}

std::vector<specification::Signature> specification::Signature::getContinuesParts() const
{
	std::vector<Signature> result;
	const auto indexes = _indexes.getContinuesParts();
	std::transform(indexes.begin(), indexes.end(), std::back_inserter(result),
			[this](const auto & indexes)
			{
				return this->operator[](indexes);
			});

	return result;
}
