#include <numeric>
#include "../Instruction.hpp"
#include "../Morpheme.hpp"
#include "../Operand.hpp"
#include "../OperandCollection.hpp"

bool isMorpheme(const specification::TemplatePtr & ptr)
{
    return std::dynamic_pointer_cast<specification::Morpheme>(ptr) != nullptr;
}

bool compare(const specification::TemplatePtr & left, const specification::TemplatePtr & right)
{
    const auto isLeftMorpheme = isMorpheme(left), isRightMorpheme = isMorpheme(right);

    if (isLeftMorpheme ^ isRightMorpheme)
        return isLeftMorpheme;

    return *(left->getIndexes() < right->getIndexes());
}

size_t specification::Instruction::weight() const
{
    return std::accumulate(_templates.begin(), _templates.end(), 0,
            [](size_t value, const TemplatePtr & temp){ return value + temp->size();});
}

bool specification::Instruction::operator<(const specification::Instruction &instruction) const
{
    return this->weight() < instruction.weight();
}

void specification::Instruction::compressOperands(std::set<specification::TemplatePtr, specification::TemplateCompare> & templates)
{
    for (auto it = templates.begin(); it != templates.end(); ++it)
    {
        if (auto operand = std::dynamic_pointer_cast<Operand>(*it); operand)
        {
            auto collection = OperandCollection::create();

            templates.erase(templates.begin(), it);
            templates.insert(collection);
            return;
        }
    }
}

specification::Instruction::Instruction(const specification::TemplateSet & templates, const specification::Context & context)
: _templates(templates), _context(context) {}

std::string specification::Instruction::getMnemonic() const
{
    for (const auto & _template: _templates)
        if (const auto morpheme = std::dynamic_pointer_cast<Morpheme>(_template); morpheme)
            if (!morpheme->isConstant())
                return *morpheme->toString();

    return "";
}

specification::Signature specification::Instruction::getSignature() const
{
    return std::accumulate(_templates.begin(), _templates.end(),
            Signature{}, [](const Signature & acc, const TemplatePtr & val)
            {
                return acc + val->getSignature();
            });

}

specification::Signature specification::Instruction::getNormalizeSignature() const
{
    auto signature = getSignature();
    Indexes lost;
    for (const auto index: getIndexes())
    {
        try
        {
            signature[index];
        }
        catch (std::out_of_range&)
        {
            lost = lost + index;
        }
    }
    signature = signature + Signature{lost, 0x0};

    return signature;
}

specification::Indexes specification::Instruction::getIndexes() const
{
    return std::accumulate(_templates.begin(), _templates.end(),
            Indexes{}, [](const Indexes & acc, const TemplatePtr & val)
                {
                    return acc + val->getIndexes();
                });
}

