#ifndef DECODER_OPERAND_COLLECTION_HPP
#define DECODER_OPERAND_COLLECTION_HPP

#include "ITemplate.hpp"
#include "Operand.hpp"

namespace specification
{
	using TemplateSet = std::set<TemplatePtr>;

	class OperandCollection : public std::set<Operand>, public ITemplate
	{
	public:
		using Ptr = std::shared_ptr<OperandCollection>;
		
		static OperandCollection::Ptr create();
		template <class Iterator, class U = typename std::iterator_traits<Iterator>::value_type>
		static OperandCollection::Ptr create(Iterator begin, Iterator end);

	public:
		OperandCollection();

		std::variant<Indexes, bool> check(const specification::Signature &signature) const override;
		
		Signature getSignature() const override;
		
		Indexes getIndexes() const override;
		
		bool isEqual(const TemplatePtr &other) override;

		bool isIntersect(const TemplatePtr &other) override;

		bool operator-=(const Indexes &other) override;
		
		size_t operandsSize() const;

		size_t size() const override;

		size_t getWeight() const;
	};
}
namespace std
{
	template <>
	class hash<specification::OperandCollection>
	{
	public:
		size_t operator()(const specification::OperandCollection & collection)
		{
			hash<specification::Operand> hash_part;
			
			if (collection.empty())
				return 0;
			
			return std::accumulate(std::next(collection.begin()), collection.end(),
			                       hash_part(*collection.begin()),
			                       [&hash_part](auto accumulator, const auto &part)
			                       {
				                       return hash_combine(accumulator, hash_part(part));
			                       });
		}
	};
}
#endif //DECODER_OPERAND_COLLECTION_HPP
