#ifndef DECODER_MORPHEME_H
#define DECODER_MORPHEME_H

#include <map>
#include <variant>

#include "ITemplate.hpp"
#include "Signature.hpp"

namespace specification
{
    class Morpheme : public ITemplate
    {
    public:
	    using Ptr = std::shared_ptr<Morpheme>;

    public:
	    enum Position { Prefix, Postfix, Affix };

    public:
        explicit Morpheme(const Signature &signature, std::string_view morpheme, Position position);
        explicit Morpheme(const Signature & signature);
	
	    size_t size() const override;

        std::variant<Indexes, bool> check(const Signature &signature) const override;

	    bool isIntersect(const ITemplate::Ptr &other) override;
	    bool isEqual(const ITemplate::Ptr &other) override;
	    bool isConstant() const;

	    bool operator-=(const Indexes &other) override;
	    Signature operator^(ITemplate::Ptr other) const;
	    void operator^=(const Indexes &other);

	    Morpheme operator+(const Morpheme & other) const;
	    Morpheme operator+(const Ptr &other) const;

	    Signature getSignature() const override;

	    Indexes getIndexes() const override;
	    std::optional<Position> getPosition() const;
	    std::optional<std::string> toString() const;

	    friend Morpheme::Ptr operator+(const Morpheme::Ptr & left, const Morpheme::Ptr & right);

    private:
	    std::optional<std::string>  _mnemonic;
        std::optional<Position>     _position;

        Signature                   _constant;
    };
}

namespace std
{
	template <>
	class hash<specification::Morpheme>
	{
	public:
		size_t operator()(const specification::Morpheme & morpheme)
		{
			std::hash<std::optional<string>> hash_str;
			std::hash<std::optional<specification::Morpheme::Position>> hash_pos;
			std::hash<specification::Signature> hash_sig;

			size_t hash = hash_sig(morpheme.getSignature());
			hash = hash_combine(hash, hash_str(morpheme.toString()));
			hash = hash_combine(hash, hash_pos(morpheme.getPosition()));
			return hash;
		}
	};
}
#endif //DECODER_MORPHEME_H
