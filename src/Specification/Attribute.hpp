#ifndef DECODER_ATTRIBUTE_HPP
#define DECODER_ATTRIBUTE_HPP

#include <string>

namespace specification
{
    using Attribute = uint64_t;
}

#endif //DECODER_ATTRIBUTE_HPP
