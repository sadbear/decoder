
#define CATCH_CONFIG_MAIN

#include "../../Service/catch.hpp"

#include <random>
#include "../Operand.hpp"
#include "../OperandCollection.hpp"

using namespace specification;

class OperandFactory
{
public:
    OperandFactory() : _engine(_rd())
    {}

    Attribute generateRandomAttribute()
    {
        return _attributeGenerator(_engine);
    }

    Operand generateRandomOperand()
    {
        std::uniform_int_distribution<uint> operandTableSize(5, 10);
        std::uniform_int_distribution<uint> indexStart(0, 64), step(5, 10);

        std::vector<std::pair<Attribute, OperandPart::Content>> parts(operandTableSize(_engine));

        uint current = indexStart(_engine);

        std::generate(parts.begin(), parts.end(),
                      [this, &current, &step]() -> std::pair<Attribute, OperandPart::Content>
                      {
                          const auto start = current;
                          current += step(_engine);
                          return {generateRandomAttribute(), OperandPart::Content{Indexes::getRange(start, current)}};
                      });

        return Operand(generateRandomAttribute(), parts);
    }

    OperandCollection generateRandomOperandCollection(size_t size)
    {
        OperandCollection collection;

        while (size-- != 0)
            collection.insert(generateRandomOperand());

        return collection;
    }

    std::mt19937 getEngine() const
    {
        return _engine;
    }

private:
    std::random_device _rd;
    std::mt19937 _engine;

    inline static auto _attributeGenerator = std::uniform_int_distribution<specification::Attribute>(1, 100);
};

TEST_CASE("Operands", "")
{
    OperandFactory factory;
    SECTION("Simple hash")
    {
        auto count = 10000;
        std::hash<Operand> hasher;
        while (count-- != 0)
        {
            auto operand = factory.generateRandomOperand();

            auto hash1 = hasher(operand);
            auto hash2 = hasher(operand);
            REQUIRE(hash1 == hash2);
        }
    }

    SECTION("Simple collection create")
    {
        auto count = 100;

        while (count-- != 0)
        {
            auto collection = factory.generateRandomOperandCollection(100);
            REQUIRE(collection.size() == 100);
        }
    }

    SECTION("Simple collection hash")
    {
        auto count = 100;
        std::hash<OperandCollection> hasher;
        std::set<size_t> hashSet;

        while (count-- != 0)
        {
            auto collection = factory.generateRandomOperandCollection(100);
            const auto hash = hasher(collection);
            REQUIRE(hash == hasher(collection));
            REQUIRE(hashSet.find(hash) == hashSet.end());

            hashSet.insert(hash);
        }
    }

    SECTION("Collection shuffle")
    {
        size_t size = 1000;

        std::vector<Operand> operands;

        while (size-- != 0)
            operands.push_back(factory.generateRandomOperand());

        size_t repeat = 1000;
        std::hash<OperandCollection> hasher;
        size_t hash = 0;

        while (repeat-- != 0)
        {
            OperandCollection collection;
            std::for_each(operands.begin(), operands.end(),
                    [&collection](const auto & operand) {collection.insert(operand);});

            if (hash == 0)
                hash = hasher(collection);

            REQUIRE(hasher(collection) == hash);
        }
    }
}
