#include <set>
#include <random>

#include "../../Service/catch.hpp"
#include "../Indexes.hpp"

TEST_CASE("Indexes", "[specification]")
{
    using namespace specification;

    std::random_device rd;
    std::mt19937 mt(rd());

    std::uniform_int_distribution<uint> dist(0, UINT16_MAX);

    auto getRandomSeparatedIndexes = [&mt](size_t size = 100, uint minimum = 0, uint limiter = UINT16_MAX) -> Indexes
    {
        std::uniform_int_distribution<uint> dist(minimum, limiter);
        std::set<uint> result;
        while (size-- != 0)
            result.insert(dist(mt));
        return Indexes(result.begin(), result.end());
    };

    auto get_random_continues_indexes = [&dist, &mt](size_t size = 100) -> Indexes
    {
        std::set<uint> result;
        auto begin = dist(mt), end = dist(mt);

        if (begin > end)
            std::swap(begin, end);

        for (auto index = begin; index != end; ++index)
            result.insert(index);

        return Indexes(result.begin(), result.end());
    };

    SECTION("Creating")
    {
        auto indexes = Indexes({0,1,2,3,4});
        REQUIRE(indexes.back() == 4);
        REQUIRE(indexes.front() == 0);
        REQUIRE_FALSE(indexes.empty());
    };

    SECTION ("isSeparated")
    {
        auto continues = get_random_continues_indexes(),
			 separated = getRandomSeparatedIndexes();

        REQUIRE(separated.isSeparated());
        REQUIRE_FALSE(continues.isSeparated());
    }

    SECTION("CompareUninterrupted")
    {
        const auto head     = dist(mt),
				   middle   = head   + dist(mt),
				   tail     = middle + dist(mt),
				   end      = tail   + dist(mt);

		const auto indexes1 = Indexes::getRange(head, middle + 1); // [head, middle]
		const auto indexes2 = Indexes::getRange(middle, tail); // [middle, tail)

        REQUIRE((indexes1 < indexes2) == std::nullopt);
        REQUIRE((indexes1 > indexes2) == std::nullopt);
        REQUIRE((indexes2 < indexes1) == std::nullopt);
        REQUIRE((indexes2 > indexes1) == std::nullopt);

        const auto indexes3 = Indexes::getRange(head, middle); // [head, middle)
        REQUIRE(*(indexes3 < indexes2)); // [head, middle) < [middle, tail)
        REQUIRE(*(indexes2 > indexes3)); // [middle, tail) > [head, middle)
        REQUIRE_FALSE(*(indexes2 < indexes3));
        REQUIRE_FALSE(*(indexes3 > indexes2));

        const auto indexes4 = Indexes::getRange(tail, end); // [tail, end)
        REQUIRE(*(indexes1 < indexes4));
        REQUIRE(*(indexes2 < indexes4));
        REQUIRE(*(indexes3 < indexes4));

        REQUIRE(*(indexes4 > indexes1));
        REQUIRE(*(indexes4 > indexes2));
        REQUIRE(*(indexes4 > indexes3));
    }

    SECTION("CompareSeparated")
    {
        auto left = getRandomSeparatedIndexes(10, 0, 100), right = getRandomSeparatedIndexes(10, 100, 200);
        REQUIRE(*(left < right));
        REQUIRE(*(right > left));

        left += right.front();
        REQUIRE((left < right) == std::nullopt);
        REQUIRE((left > right) == std::nullopt);
    }

    SECTION("Sum")
    {
        auto indexes1 = Indexes({0,1,4}),
             indexes2 = Indexes({5,10,11});

        REQUIRE((indexes1 + indexes2) == std::set<uint>{0, 1, 4, 5, 10, 11});
    }

    SECTION("Shrink indexes")
    {
        auto indexes = Indexes{0,1,2,5};
        indexes.shrinkToContinuous();
        REQUIRE(indexes == Indexes{0,1,2});
    }

    SECTION("Intersect")
    {
        const auto start = dist(mt),
			middle = start + dist(mt),
			end = middle + dist(mt);

        const auto indexes1 = Indexes::getRange(start, end), /*[start, end)*/
				   indexes2 = Indexes::getRange(middle, end); /*[middle, end)*/

        const auto intersected = indexes1 ^ indexes2; // [middle, end)

        REQUIRE(!intersected.isSeparated());
        REQUIRE(intersected.front() == middle);
        REQUIRE(intersected.back() + 1 == end);

		// ToDo Add tests that simulate problems from the pulling of nodes of morphemes
    }
}