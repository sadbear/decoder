#include "../../Service/catch.hpp"
#include "../Morpheme.hpp"

using namespace specification;

TEST_CASE ("Morpheme tests", "[specification]")
{
    SECTION("Create normal morpheme")
    {
        auto morpheme = Morpheme(
                {Indexes::getRange(0, 4), 0x0},
                "PREFIX",
                Morpheme::Prefix);
        REQUIRE_FALSE(morpheme.isConstant());
        REQUIRE_NOTHROW(morpheme.toString());
        REQUIRE_NOTHROW(morpheme.getPosition());
    }

    SECTION("Create constant")
    {
        auto morpheme = Morpheme({{0,1}, {0,0}, {0,1}});
        REQUIRE(morpheme.isConstant());
        REQUIRE(morpheme.toString() == std::nullopt);
        REQUIRE(morpheme.getPosition() == std::nullopt);
    }

    SECTION("Check morpheme")
    {
        auto morpheme = Morpheme({Indexes::getRange(0, 16), 0xf00f}, "PREFIX", Morpheme::Prefix);
        auto result = morpheme.check({Indexes::getRange(0, 4), 0xf});

        REQUIRE_THROWS(std::get<bool>(result));

        auto intersect = std::get<Indexes>(result);
        REQUIRE(intersect == Indexes::getRange(0, 4));

    }

    SECTION("Partition morpheme check")
    {
        auto morpheme =
                Morpheme(
					{Indexes::getRange(0, 16), 0xf00f},
					"PREFIX",
					Morpheme::Prefix
        		);

	    auto result = morpheme.check({Indexes::getRange(0, 16), 0x00df});
        REQUIRE_THROWS(std::get<bool>(result));

        auto intersect = std::get<Indexes>(result);
        REQUIRE(intersect == Indexes{0, 1, 2, 3, 5, 8, 9, 10, 11});
    }
}