#include "../../Service/catch.hpp"
#include "../Signature.hpp"

using namespace specification;

TEST_CASE("Signature class tests", "[specification]")
{
    SECTION("Create")
    {
        REQUIRE_NOTHROW(Signature({0,1,2,3}, {0,0,0,1}));

        REQUIRE_THROWS(Signature({0}, {0,1}));
        REQUIRE_THROWS(Signature({0, 1}, std::vector<bool>{0}));

        SECTION("Create signature by uint")
        {
            auto signature = Signature(Indexes::getRange(0, 16), 0xf0f0);
            REQUIRE(signature[Indexes::getRange(0, 4)].getRaw() == std::vector<bool>{0,0,0,0});
            REQUIRE(signature[Indexes::getRange(4, 8)].getRaw() == std::vector<bool>{1,1,1,1});
            REQUIRE(signature[Indexes::getRange(8, 12)].getRaw() == std::vector<bool>{0,0,0,0});
            REQUIRE(signature[Indexes::getRange(12, 16)].getRaw() == std::vector<bool>{1,1,1,1});
        }

        // ToDo Add more tests for `uint`->`vector<bool>` creating
    }

    SECTION("ToInt")
    {
        REQUIRE(Signature{Indexes::getRange(0, 4), 0xe}.toInt() == 0xe);
    }

    SECTION("Operator [uint]")
    {
        auto signature = Signature({{0,0}, {1,1}, {2,0}, {3,1}, {4,0}});

        bool bit = false;
        for (auto index = 0; index != signature.size(); ++index)
        {
            REQUIRE(bit == signature[index]);
            bit = !bit;
        }
    }

    SECTION("Operator [Indexes]")
    {
        const auto indexes = Indexes::getRange(0, 10);
        const auto signature = Signature(indexes, {1, 1, 1, 1, 1, 0, 0, 0, 0, 0});
        REQUIRE(signature[Indexes::getRange(0, 5)].getRaw() ==  std::vector<bool>{1,1,1,1,1});
        REQUIRE(signature[Indexes::getRange(5, 10)].getRaw() == std::vector<bool>{0,0,0,0,0});
    }

    SECTION("Intersect")
    {
        const auto left  = Indexes::getRange(0, 12),
                   right = Indexes::getRange(4, 16);

        const auto leftS  = Signature{left, 0xff0},
                   rightS = Signature{right, 0x0ff};

        const auto intersected = leftS ^rightS;

        REQUIRE(intersected.getIndexes() == Indexes::getRange(4, 12));
        REQUIRE(intersected.toInt() == 255);

    }
    // ToDo Add more tests
}