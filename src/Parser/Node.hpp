#ifndef DECODER_NODE_HPP
#define DECODER_NODE_HPP

#include <functional>
#include <memory>
#include <set>
#include "../Specification/ITemplate.hpp"
#include "../Storages/ISignatureSource.h"

namespace parser
{
	enum Trinity {R_TRUE, R_FALSE, R_MAYBE};
	
    template <class ResultType>
    using Result = std::pair<Trinity, ResultType>;
	using NodeProcessor = std::function<void(specification::TemplatePtr)>;

    class Node
    {
    public:
        using Ptr = std::shared_ptr<Node>;
        using Compare = std::function<bool(const Ptr & left, const Ptr & right)>;
	    using Vector = std::vector<Ptr>;

    public:
        Node();

        virtual int getId() const;

        virtual void traversal(std::function<void(const Ptr &)> callback) const;

    public:

	    virtual void compress() = 0;
	    virtual std::optional<specification::TemplatePtr> getTemplate() const;
	    virtual Result<Node::Ptr> find(const specification::CheckContextFunc &context,
	                                   storage::ISignatureSource::Ptr signature_source,
	                                   const NodeProcessor &callback) = 0;

	    virtual bool check(storage::ISignatureSource::Ptr signature,
	                       const specification::CheckContextFunc &context) = 0;

        virtual Ptr pushNode(const Node::Ptr &node) = 0;

        virtual size_t getWeight() const = 0;

        virtual bool isFullPath() const = 0;

	    virtual bool contains(Ptr other) const = 0;

	    virtual const Vector &getChildren() const;

	    virtual void appendChildren(const Ptr &child);

    public:
	    Vector _children;
        int    _id;

    protected:
	    static int id_ptr;
    };
}

#endif //DECODER_NODE_HPP
