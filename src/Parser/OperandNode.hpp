#ifndef DECODER_OPERANDS_DECODER_HPP
#define DECODER_OPERANDS_DECODER_HPP


#include <memory>

#include "../Specification/Operand.hpp"
#include "../Specification/OperandCollection.hpp"

#include "Node.hpp"

namespace parser
{
    class OperandNode : public Node
    {
    public:
        using Ptr = std::shared_ptr<OperandNode>;

        static Node::Ptr create(const specification::OperandCollection::Ptr & collection);

    public:
        OperandNode();

        explicit OperandNode(const specification::OperandCollection::Ptr &collection);

        Result<Node::Ptr> find(const specification::CheckContextFunc &context, storage::ISignatureSource::Ptr signature_source, const NodeProcessor &callback) override;

        bool check(storage::ISignatureSource::Ptr signature, const specification::CheckContextFunc &context) override;

        Node::Ptr pushNode(const Node::Ptr &node) override;

        size_t getWeight() const override;
        
        bool isFullPath() const override;

        bool contains(Node::Ptr other) const override;

        const Vector & getChildren() const override;

        void compress() override;

        std::optional<specification::TemplatePtr> getTemplate() const override;

        void appendChildren(const Node::Ptr &child) override;

    private:
        specification::OperandCollection::Ptr _operands;
    };
}

#endif //DECODER_OPERANDS_DECODER_HPP
