#ifndef DECODER_CONTEXT_DECODER_HPP
#define DECODER_CONTEXT_DECODER_HPP


#include "Node.hpp"
#include "../Specification/Context.hpp"
#include "../Storages/ISignatureSource.h"
#include "../Specification/ITemplate.hpp"

namespace parser
{
	class ContextNode : public Node
    {
    public:
        using Ptr = std::shared_ptr<ContextNode>;

        static Node::Ptr create(const specification::Context & context = {});

    public:
        ContextNode();

		explicit ContextNode(const specification::Context & context);

        Node::Ptr pushNode(const Node::Ptr &node) override;

		bool check(storage::ISignatureSource::Ptr signature, const specification::CheckContextFunc &context) override;

		bool contains(Node::Ptr other) const override;

		bool contains(const specification::Context &context) const;

        Result<Node::Ptr> find(const specification::CheckContextFunc & context,
							   storage::ISignatureSource::Ptr signature_source,
							   const NodeProcessor & callback) override;

        bool isFullPath() const override;

        size_t getWeight() const override;

		std::vector<Ptr> getContextChildren();
        const specification::Context & getContext() const;

        bool operator==(const ContextNode & other) const;

		void compress() override;

	private:
        specification::Context _context;
    };
}

#endif //DECODER_CONTEXT_DECODER_HPP
