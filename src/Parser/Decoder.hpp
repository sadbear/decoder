#ifndef DECODER_DECODER_HPP
#define DECODER_DECODER_HPP

#include "../Specification/Instruction.hpp"
#include "MorphemeNode.hpp"
#include "OperandNode.hpp"
#include "Node.hpp"
#include "ContextNode.hpp"

namespace parser
{
	using TemplateProcess = std::function<void(specification::TemplatePtr)>;

    class Decoder
    {
    public:
        explicit Decoder(std::vector<specification::Instruction> instructions);

	public:
        std::map<specification::Instruction, Node::Ptr> initializeContextNodes(const std::vector<specification::Instruction> & instructions);

	    void initializeTemplatesNode(const std::map<specification::Instruction, Node::Ptr> & instructions);

	    void find(specification::CheckContextFunc & contextCheck,
				  storage::ISignatureSource::Ptr & source,
				  TemplateProcess & processor) const;

	    void traversal(std::function<void(const Node::Ptr & node)> & callback) const;

        void compress();

    private:
	    std::unordered_map<specification::TemplatePtr, Node::Ptr> _hashTable;
	    Node::Ptr _root;
    };
}

#endif //DECODER_DECODER_HPP
