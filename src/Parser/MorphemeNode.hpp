#ifndef DECODER_MORPHEME_DECODER_HPP
#define DECODER_MORPHEME_DECODER_HPP


#include <memory>
#include "Node.hpp"
#include "../Specification/Morpheme.hpp"

namespace parser
{
	class MorphemeNode : public Node
	{
	public:
		using Ptr = std::shared_ptr<MorphemeNode>;
		
		static Node::Ptr create(const specification::Morpheme::Ptr &morpheme, bool isFullPath = false);
	
	public:
		explicit MorphemeNode(const specification::Morpheme::Ptr &morpheme, bool isFullPath = false);
		
		Result<Node::Ptr> find(
				const specification::CheckContextFunc &context,
				storage::ISignatureSource::Ptr signature_source,
				const NodeProcessor &callback
		) override;
		
		bool
		check(storage::ISignatureSource::Ptr signature_source, const specification::CheckContextFunc &context) override;
		
		bool check(const specification::Signature &signature);
		
		const specification::Morpheme::Ptr &getMorpheme() const;
		
		std::vector<Ptr> getMorphemeNodeChildren() const;
		
		void crop(const specification::Indexes &signature);
		
		bool contains(Node::Ptr other) const override;
		bool contains(const specification::Signature &other) const;
		
		Node::Ptr pushNode(const Node::Ptr &node) override;
		Node::Ptr pullNode(specification::Signature signature);
		
		size_t getWeight() const override;
		
		bool isFullPath() const override;
		
		std::optional<specification::TemplatePtr> getTemplate() const override;
		
		void compress() override;
	
	private:
		bool _full;
		specification::Morpheme::Ptr _morpheme;
	};
}

#endif //DECODER_MORPHEME_DECODER_HPP
