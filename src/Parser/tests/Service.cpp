#include "Service.hpp"

#include <random>
#include <vector>
#include <string>


std::string getRandomString(size_t length)
{
	std::random_device rd;
	std::mt19937 mt(rd());

	const char charset[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	const size_t maxIndex = (sizeof(charset) - 1);
	std::uniform_int_distribution<int> dist(0, maxIndex - 1);

	auto randChar = [&mt, &charset, &maxIndex, &dist]() -> char
	{
		const int index = dist(mt);
		return charset[index];
	};

	std::string str(length, 0);
	std::generate_n(str.begin(), length, randChar);
	return str;
}

std::vector<specification::Morpheme::Ptr>
createSimpleMorphemes(std::string_view morpheme, uint64_t constant, uint signatureStep, size_t instruction_size)
{
	if (instruction_size % signatureStep != 0)
		throw std::logic_error("Step is not a multiple of 64");

	using namespace specification;
	std::vector<Morpheme::Ptr> morphemes;

	auto signature = Signature{Indexes::getRange(0, uint(instruction_size)), constant};
	auto indexes = Indexes::getRange(0, signatureStep);

	while (indexes.back() < instruction_size)
	{
		morphemes.push_back(std::make_shared<Morpheme>(signature ^ indexes));

		indexes = Indexes::getRange(indexes.back() + 1, indexes.back() + signatureStep + 1);
	}

	const auto lastSignature = morphemes.back()->getSignature();
	morphemes.pop_back();

	morphemes.push_back(std::make_shared<Morpheme>(lastSignature, morpheme, Morpheme::Affix));
	return morphemes;
}

void push(parser::Node::Ptr &root, std::vector<specification::Morpheme::Ptr> morphemes)
{
	auto ptr = root;
	for (const auto &morpheme: morphemes)
	{
		auto tmp = ptr->pushNode(parser::MorphemeNode::create(morpheme, !morpheme->isConstant()));
		if (tmp != nullptr)
			ptr = tmp;
	}
}
