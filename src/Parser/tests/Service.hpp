#ifndef DECODER_SERVICE_FUNCTIONS_HPP
#define DECODER_SERVICE_FUNCTIONS_HPP

#include <string>
#include <random>

#include "../MorphemeNode.hpp"
#include "../ContextNode.hpp"

std::string getRandomString(size_t length);

std::vector<specification::Morpheme::Ptr>
createSimpleMorphemes(std::string_view morpheme, uint64_t constant, uint signatureStep, size_t instruction_size = 64);

void push(parser::Node::Ptr &root, std::vector<specification::Morpheme::Ptr> morphemes);

#endif //DECODER_SERVICE_FUNCTIONS_HPP
