#include <filesystem>
#include <fstream>
#include <memory>
#include <random>
#include <bitset>
#include <stack>

#include "../../Storages/BlankStorage.h"
#include "../../Service/catch.hpp"

#include "../MorphemeNode.hpp"
#include "../ContextNode.hpp"

#include "Service.hpp"


void write_text(const std::vector<std::pair<std::string, uint16_t>> &instructions, std::string_view filename)
{
	std::filesystem::path work_dir = "/home/sad/.decoder";
	std::ofstream stream(work_dir / filename);
	std::stringstream constants;
	constants << "std::stack<uint16_t> constants = {";
	for (const auto &[mnemonic, constant_]: instructions)
	{
		auto constant = std::bitset<16>(constant_);
		
		stream << mnemonic << ": 0x" << std::hex << constant_ << std::dec << " :";
		for (auto index = 15; index != 0; index--)
			stream << constant[index];
		stream << std::endl;
		
		constants << "0x" << std::hex << constant_ << ",";
	}
	constants.seekp(-1, std::stringstream::cur);
	constants << "};";
	stream << constants.str();
	stream.close();
}

TEST_CASE("Morpheme nodes debug", "[parser]")
{
	using namespace specification;
	using namespace parser;
	
	auto cast = [](Node::Ptr node) -> MorphemeNode::Ptr
	{
		return std::dynamic_pointer_cast<MorphemeNode>(node);
	};
	
	auto create_morpheme = [](
			const Indexes &indexes,
			uint64_t constant,
			std::string_view morpheme,
			Morpheme::Position pos
	) -> Node::Ptr
	{ return MorphemeNode::create(std::make_shared<Morpheme>(Signature{indexes, constant}, morpheme, pos)); };
	
	auto create_constant = [](
			const Indexes &indexes,
			uint64_t constant
	) -> Node::Ptr
	{ return MorphemeNode::create(std::make_shared<Morpheme>(Signature{indexes, constant})); };
	
	std::random_device rd;
	std::mt19937 mt(rd());
	
	std::uniform_int_distribution<uint16_t> dist(0, 0xffff);
	std::function<uint16_t()> generate_constant = [&rd, &mt, &dist]() -> uint16_t
	{
		const uint16_t prefix = 0x0001, mask = 0xffff ^ prefix;
		
		return (dist(mt) & mask) | prefix;
	};
	
	std::stack<uint16_t> constants;
	for (auto val : {0xab8f,0xdccf,0xb5cf,0x172f,0xc76f,0x19df,0x763f,0x887f,0x467f,0xdb7f})
		constants.push(val);
	
	std::function<uint16_t()> get_constants = [&constants]() -> uint16_t
	{
		const auto top = constants.top();
		constants.pop();
		return top;
	};
	
	auto generate = [&rd, &mt](std::function<uint16_t()> & generator, const int count) -> std::vector<std::pair<std::string, uint16_t>>
	{
		std::vector<std::pair<std::string, uint16_t>> instructions;
		for (auto _ = 0; _ != count; ++_)
		{
			const auto constant = generator();
			instructions.push_back({getRandomString(10), constant});
		}
		std::sort(instructions.begin(), instructions.end(),
		          [](const auto &left_, const auto &right_)
		          {
			          auto left = std::bitset<16>(left_.second).to_string();
			          auto right = std::bitset<16>(right_.second).to_string();
					  std::reverse(left.begin(), left.end());
					  std::reverse(right.begin(), right.end());
			          return left < right;
		          });
		return instructions;
	};
	
	std::list<uint16_t> _constants = {0x32cf, 0x556f, 0x759f, 0xae3f, 0xee9f};
	
	std::vector<std::pair<std::string, uint16_t>> instructions;
	instructions.resize(_constants.size());
	auto index = 0;
	std::generate(instructions.begin(), instructions.end(),
			[&_constants, &index] () -> std::pair<std::string, uint16_t>
			{
				std::pair<std::string, uint16_t> result = {std::to_string(index++) + "_" + getRandomString(3), _constants.back()};
				_constants.pop_back();
				return result;
			});
	
	SECTION("Lost indexes")
	{
		auto root = ContextNode::create();

		const auto instructions = generate(generate_constant, 50);
		
		int index = 0;
		write_text(instructions, "lost_indexes.txt");
		for (const auto &[mnemonic, constant]: instructions)
		{
			const auto morphemes = createSimpleMorphemes(mnemonic, constant, 4, 16);
			
			auto ptr = root;
			auto inner_index = 0;
			for (const auto &morpheme: morphemes)
			{
				const auto label =
						"lost_indexes_" + std::to_string(index) + "_" + std::to_string(inner_index++) + ".json";
				auto tmp = ptr->pushNode(parser::MorphemeNode::create(morpheme, !morpheme->isConstant()));
//				write_png(root, label);
				if (tmp != nullptr)
					ptr = tmp;
			}
			++index;
		}
		root->compress();
	}
}

TEST_CASE("Morpheme nodes")
{
	using namespace specification;
	using namespace parser;
	
	auto cast = [](Node::Ptr node) -> MorphemeNode::Ptr
	{
		return std::dynamic_pointer_cast<MorphemeNode>(node);
	};
	
	auto create_morpheme = [](
			const Indexes &indexes, uint64_t constant, std::string_view morpheme,
			Morpheme::Position pos
	) -> Node::Ptr
	{
		return MorphemeNode::create(std::make_shared<Morpheme>(Signature{indexes, constant}, morpheme, pos));
	};
	
	auto create_constant = [](const Indexes &indexes, uint64_t constant) -> Node::Ptr
	{
		return MorphemeNode::create(std::make_shared<Morpheme>(Signature{indexes, constant}));
	};
	
	std::filesystem::path work_dir = "/home/sad/.decoder";
	
	std::random_device rd;
	std::mt19937 mt(rd());
	
	SECTION("Create Morpheme")
	{
		const auto signature = Signature{{0, 1},
		                                 {1, 1},
		                                 {2, 3}};
		const auto mnemonic = "Morpheme";
		
		const auto morpheme = std::make_shared<Morpheme>(signature, mnemonic, Morpheme::Affix);
		const auto node = MorphemeNode::create(morpheme);
		
		MorphemeNode::Ptr morphemeNode = std::dynamic_pointer_cast<MorphemeNode>(node);
		REQUIRE(morphemeNode);
		REQUIRE(morphemeNode->getMorpheme()->getSignature() == signature);
		REQUIRE(*morphemeNode->getMorpheme()->toString() == mnemonic);
	}
	
	SECTION("Simple Pull")
	{
		auto root = ContextNode::create();
		
		
		// Simple add {0..8}, 0xff
		{
			root->pushNode(create_constant(Indexes::getRange(0, 4), 0xf))
					->pushNode(create_morpheme(Indexes::getRange(4, 8), 0xf, "1", Morpheme::Affix));
			
			REQUIRE(root->getChildren().size() == 1);
			
			// Head
			auto node = cast(root->getChildren()[0]);
			REQUIRE(node->getMorpheme()->getSignature().toInt() == 0xf);
			REQUIRE(node->getChildren().size() == 1);
			
			// Tail
			node = cast(node->getChildren()[0]);
			REQUIRE(node->getMorpheme()->getSignature().toInt() == 15); // ToDo
			REQUIRE(node->getChildren().empty());
		}
		
		// Adding existing morpheme.
		{
			root->pushNode(create_constant(Indexes::getRange(0, 4), 0xf));
			// Head
			auto node = cast(root->getChildren()[0]);
			REQUIRE(node->getMorpheme()->getSignature().toInt() == 0xf);
			REQUIRE(node->getChildren().size() == 1);
			
			// Tail
			node = cast(node->getChildren()[0]);
			REQUIRE(node->getMorpheme()->getSignature().toInt() == 15); // ToDo
			REQUIRE(node->getChildren().empty());
			REQUIRE(root->getChildren().size() == 1);
		}
		
		// Add branch to tail
		{
			root->pushNode(create_constant(Indexes::getRange(0, 4), 0xf))
					->pushNode(create_morpheme(Indexes::getRange(4, 8), 0xe, "0", Morpheme::Postfix));
			
			REQUIRE(root->getChildren().size() == 1);
			
			// Head
			auto node = cast(root->getChildren()[0]);
			REQUIRE(node->getMorpheme()->getSignature().toInt() == 0xf);
			
			// Middle
			node = cast(node->getChildren()[0]);
			REQUIRE(node->getChildren().size() == 2);
			REQUIRE(node->getMorpheme()->getIndexes() == Indexes{5, 6, 7});
			REQUIRE(node->getMorpheme()->getSignature().getRaw() == std::vector<bool>{1, 1, 1});
			
			// Tail
			const std::vector<MorphemeNode::Ptr> nodes =
					{
							cast(node->getChildren()[0]),
							cast(node->getChildren()[1])
					};
			
			REQUIRE(nodes[0]->getMorpheme()->getIndexes() == Indexes{4});
			REQUIRE(nodes[0]->getMorpheme()->getSignature().getRaw() == std::vector<bool>{1});
			REQUIRE(nodes[0]->getMorpheme()->toString() == "1");
			
			REQUIRE(nodes[1]->getMorpheme()->getIndexes() == Indexes{4});
			REQUIRE(nodes[1]->getMorpheme()->getSignature().getRaw() == std::vector<bool>{0});
			REQUIRE(nodes[1]->getMorpheme()->toString() == "0");
		}
	}
	
	SECTION("Pull")
	{
		const auto indexes = Indexes::getRange(0, 8);
		auto root = ContextNode::create();
		
		
		root->pushNode(MorphemeNode::create(
						std::make_shared<Morpheme>(Signature{indexes, 0xe1}, "ONE", Morpheme::Affix)))
				->pushNode(MorphemeNode::create(std::make_shared<Morpheme>(Signature{Indexes::getRange(9, 16), 0x2e})));
		
		root->pushNode(MorphemeNode::create(
						std::make_shared<Morpheme>(Signature{indexes, 0xe0}, "TWO", Morpheme::Postfix)))
				->pushNode(MorphemeNode::create(std::make_shared<Morpheme>(Signature{Indexes::getRange(9, 16), 0x2e})));
		
		
		auto children = root->getChildren();
		REQUIRE(children.size() == 1);
		
		const auto pulled = std::dynamic_pointer_cast<MorphemeNode>(children[0]);
		REQUIRE(pulled);
		REQUIRE(pulled->getMorpheme()->getSignature().toInt() == 112); // ToDo
		REQUIRE(pulled->getMorpheme()->getSignature().getIndexes() == Indexes{1, 2, 3, 4, 5, 6, 7});
		REQUIRE(pulled->getMorpheme()->toString() == std::nullopt);
		
		children = pulled->getChildren();
		REQUIRE(children.size() == 2);
		
		auto one = std::dynamic_pointer_cast<MorphemeNode>(children[0]),
				two = std::dynamic_pointer_cast<MorphemeNode>(children[1]);
		
		REQUIRE(one);
		REQUIRE(one->getChildren().size() == 1);
		REQUIRE(one->getMorpheme()->getIndexes() == Indexes{0});
		REQUIRE(one->getMorpheme()->getSignature().toInt() == 0x1);
		REQUIRE(one->getMorpheme()->toString() == "ONE");
		REQUIRE(one->getMorpheme()->getPosition() == Morpheme::Affix);
		
		REQUIRE(two);
		REQUIRE(two->getChildren().size() == 1);
		REQUIRE(two->getMorpheme()->getIndexes() == Indexes{0});
		REQUIRE(two->getMorpheme()->getSignature().toInt() == 0x0);
		REQUIRE(two->getMorpheme()->toString() == "TWO");
		REQUIRE(two->getMorpheme()->getPosition() == Morpheme::Postfix);
	}
	
	SECTION("Big push")
	{
		std::vector<std::vector<Morpheme::Ptr>> instructions = {
				createSimpleMorphemes("0", 0x1111111111111111, 4),
				createSimpleMorphemes("1", 0x2111111111111211, 4)
		};
		
		
		auto root = ContextNode::create();
		
		// Start '0'
		{
			push(root, instructions[0]);
			
			REQUIRE(root->getChildren().size() == 1);
			auto ptr = root->getChildren()[0];
			uint index = 0;
			MorphemeNode::Ptr node;
			
			do
			{
				REQUIRE(ptr->getChildren().size() == 1);
				node = cast(ptr);
				
				REQUIRE(node->getMorpheme()->getIndexes().front() == index);
				index += 4;
				
				REQUIRE(node->getMorpheme()->getSignature().getRaw() == std::vector<bool>{1, 0, 0, 0});
				
				ptr = ptr->getChildren()[0];
				
			} while (!ptr->getChildren().empty());
			
			node = cast(ptr);
			REQUIRE(node->getMorpheme()->toString() == "0");
		}
		
		// Start '1'
		{
			push(root, instructions[1]);
			
			REQUIRE(root->getChildren().size() == 1);
			auto ptr = cast(root->getChildren()[0]);
			REQUIRE(ptr->getMorpheme()->isConstant());
			REQUIRE(ptr->getMorpheme()->getSignature().getRaw() == std::vector<bool>{1, 0, 0, 0});
			
			ptr = cast(ptr->getChildren()[0]);
			REQUIRE(ptr->getMorpheme()->isConstant());
			REQUIRE(ptr->getMorpheme()->getSignature().getRaw() == std::vector<bool>{1, 0, 0, 0});
			
			ptr = cast(ptr->getChildren()[0]);
			REQUIRE(ptr->getMorpheme()->getIndexes() == Indexes::getRange(10, 12));
			REQUIRE(ptr->getMorpheme()->getSignature().getRaw() == std::vector<bool>{0, 0});
			REQUIRE(ptr->getChildren().size() == 2);
			
			REQUIRE(cast(ptr->getChildren()[0])->getMorpheme()->getSignature().getRaw() == std::vector<bool>{1, 0});
			REQUIRE(cast(ptr->getChildren()[1])->getMorpheme()->getSignature().getRaw() == std::vector<bool>{0, 1});
		}
	}
	
	
	SECTION("Compress")
	{
		
		std::uniform_int_distribution<uint64_t> dist(0, 0xffffffffffffffff);
		
		auto root_ = ContextNode::create();
		int count = 50;
		std::vector<std::pair<uint64_t, std::string>> instructions;
		while (count-- != 0)
		{
			const auto constant = dist(mt);
			const auto morphemes = createSimpleMorphemes(getRandomString(10), constant, 4);
			
			instructions.emplace_back(constant, *morphemes.back()->toString());
			push(root_, morphemes);
		}

		root_->compress();
		
	}
	
	SECTION("Find")
	{
		std::uniform_int_distribution<uint64_t> dist(0, 0xffffffffffffffff);
		
		auto root_ = ContextNode::create();
		int count = 100;
		std::vector<std::pair<uint64_t, std::string>> instructions;
		while (count-- != 0)
		{
			const auto constant = dist(mt);
			const auto morphemes = createSimpleMorphemes(getRandomString(10), constant, 4);
			
			instructions.emplace_back(constant, *morphemes.back()->toString());
			push(root_, morphemes);
		}
		
		root_->compress();
		
		const auto check = [](const Attribute &attribute) -> int
		{ return 0; };
		for (const auto &[constant, required_mnemonic_]: instructions)
		{
			const auto required_mnemonic = required_mnemonic_; // Fixed Clion issue
			auto storage = storage::BlankStorage::create(constant);
			const auto[result, node] = root_->find(check, storage,
			                                       [&required_mnemonic](TemplatePtr tmp)
			                                       {
				                                       if (auto morpheme = std::dynamic_pointer_cast<Morpheme>(
							                                       tmp); morpheme)
					                                       if (auto mnemonic = morpheme->toString(); mnemonic)
						                                       REQUIRE(*mnemonic == required_mnemonic);
			                                       });
//			REQUIRE(result);
		}
	}
}
