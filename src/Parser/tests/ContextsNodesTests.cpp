#include "../../Service/catch.hpp"
#include "../ContextNode.hpp"
#include "../../Specification/Context.hpp"
#include "../../Storages/BlankStorage.h"


TEST_CASE ("Context Node tests", "[parser]")
{
    using namespace parser;
    using namespace specification;

    SECTION("Create by context")
    {
        auto node = ContextNode::create({{0x1, 1}, {0x2, 2}, {0x3, "Hello"}});
    }


	SECTION("Push context")
	{
		auto root = ContextNode({{0x1, 1},
		                         {0x2, 2},
		                         {0x3, 3}});

		root.pushNode(ContextNode::create({{0x1, 1},
		                                   {0x4, 4}}));
		REQUIRE(root.getContext() == Context{{0x1, 1}});

		std::vector<Node::Ptr> children(root.getChildren().begin(), root.getChildren().end());

		auto ctx0 = std::dynamic_pointer_cast<ContextNode>(children[0])->getContext(),
				ctx1 = std::dynamic_pointer_cast<ContextNode>(children[1])->getContext();

		REQUIRE(ctx0 == Context{{4, 4}});
		REQUIRE(ctx1 == Context{{2, 2},
		                        {3, 3}});
	}

	SECTION("Find")
	{
		auto root = ContextNode();
		root.pushNode(ContextNode::create(Context{{0x1, 1},  {0x2, 2},  {0x3, 3},  {0x4, 88}}));
		root.pushNode(ContextNode::create(Context{{0x1, 1},  {0x2, 12}, {0x3, 13}, {0x4, 44}}));
		const auto context = Context{{0x1, 1},  {0x2, 12}, {0x3, 23},  {0x4, 44}};
		root.pushNode(ContextNode::create(context));

		auto [result, node] =
				root.find([&context](const Attribute & attr)->std::variant<int, std::string>
				        {
							return context.at(attr);
						},
				storage::BlankStorage::create("01"), {});

		REQUIRE(result);
		auto _node = std::dynamic_pointer_cast<ContextNode>(node);
		REQUIRE(_node->getContext() == Context{{0x3, 23}});
	}
}