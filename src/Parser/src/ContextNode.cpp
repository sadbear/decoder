#include <memory>

#include "../ContextNode.hpp"
#include "../../Storages/ISignatureSource.h"
#include "../../Specification/ITemplate.hpp"
#include "../../Service/hash_combine.hpp"

using namespace parser;

parser::ContextNode::ContextNode() = default;

parser::Node::Ptr parser::ContextNode::pushNode(const Node::Ptr &node)
{
	if (auto newNode = std::dynamic_pointer_cast<ContextNode>(node); newNode)
	{
		specification::Context difference;

		std::set_difference(
				newNode->_context.begin(), newNode->_context.end(),
				_context.begin(), _context.end(),
				std::inserter(difference, difference.end()));

		if (difference.empty()) // Current node have all context
			return std::dynamic_pointer_cast<Node>(ContextNode::Ptr(this));

		if (std::includes(newNode->_context.begin(), newNode->_context.end(), _context.begin(), _context.end()))
		{
			newNode->_context = difference;
			for (const ContextNode::Ptr &child: getContextChildren())
			{
				if (child->contains(newNode->_context))
					return child->pushNode(node);
			}
		} else if (difference.size() != newNode->_context.size()) // Current node intersecting with new context
		{
			auto child = std::make_shared<ContextNode>();
			std::set_difference(
					_context.begin(), _context.end(),
					newNode->_context.begin(), newNode->_context.end(),
					std::inserter(child->_context, child->_context.end()));

			specification::Context newCurrent;
			std::set_intersection(
					_context.begin(), _context.end(),
					newNode->_context.begin(), newNode->_context.end(),
					std::inserter(newCurrent, newCurrent.end()));

			_context = newCurrent;

			std::swap(_children, child->_children);
			appendChildren(std::dynamic_pointer_cast<Node>(child));

			newNode->_context = difference;
		}
	} else
	{
		for (const auto &child: _children)
			if (child->contains(node))
			{
				auto pushed = child->pushNode(node);
				return pushed == nullptr ? child : pushed;
			}
	}
	appendChildren(node);
	return node;
}

std::vector<parser::ContextNode::Ptr> parser::ContextNode::getContextChildren()
{
	std::vector<parser::ContextNode::Ptr> result;

	for (const auto &child: _children)
		if (auto newNode = std::dynamic_pointer_cast<ContextNode>(child); newNode)
			result.push_back(newNode);

	return result;
}

size_t parser::ContextNode::getWeight() const
{
	return _context.size();
}

Result<Node::Ptr> ContextNode::find(const specification::CheckContextFunc &context,
                                    storage::ISignatureSource::Ptr signature_source,
                                    const NodeProcessor &callback)
{
	for (const auto &child: _children)
	{
		if (child->check(signature_source, context))
		{
			auto result = child->find(context, signature_source, callback);
			if (result.first)
			{
				if (!result.second)
					result.second = child;

				return result;
			}
		}
	}

	return {R_FALSE, parser::Node::Ptr()};
}

bool parser::ContextNode::check(storage::ISignatureSource::Ptr, const specification::CheckContextFunc &context)
{
	return std::any_of(_context.begin(), _context.end(),
	                   [&context](const auto &attribute_value)
	                   {
		                   return context(attribute_value.first) == attribute_value.second;
	                   });
}

bool parser::ContextNode::contains(const specification::Context &context) const
{
	for (const auto &[attr, value]: context)
		if (_context.at(attr) == value)
			return true;
	return false;
}

bool parser::ContextNode::operator==(const parser::ContextNode &other) const
{
	return _context == other._context;
}

bool parser::ContextNode::isFullPath() const
{
	return _children.empty();
}

parser::Node::Ptr parser::ContextNode::create(const specification::Context &context)
{
	return std::dynamic_pointer_cast<Node>(std::make_shared<ContextNode>(context));
}

parser::ContextNode::ContextNode(const specification::Context &context) : _context(context), Node()
{}

const specification::Context &parser::ContextNode::getContext() const
{
	return _context;
}

bool parser::ContextNode::contains(parser::Node::Ptr other) const
{
	if (auto node = std::dynamic_pointer_cast<ContextNode>(other); other)
		return contains(node->getContext());

	return false;
}

void ContextNode::compress()
{
	// ToDo Realize compress for `ContextNode`

	std::for_each(_children.begin(), _children.end(),
			[](Node::Ptr node)
			{
				node->compress();
			});
}

