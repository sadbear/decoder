#include "../Node.hpp"

using namespace parser;

int parser::Node::id_ptr = 0;

const Node::Vector & Node::getChildren() const
{
    return _children;
}

void Node::appendChildren(const Node::Ptr &newNode)
{
	const auto _comparer =
		[](const Node::Ptr & left, const Node::Ptr & right)->bool
		{
			return left->getWeight() < right->getWeight();
		};

	_children.push_back(newNode);
	std::sort(_children.begin(), _children.end(), _comparer);
}

Node::Node()
{
	_id = Node::id_ptr++;
}

int Node::getId() const
{
	return _id;
}

void Node::traversal(std::function<void(const Ptr &)> callback) const
{
	std::for_each(_children.begin(), _children.end(),
			[&callback](Node::Ptr node)
			{
				callback(node);
				node->traversal(callback);
			});
}

std::optional<specification::TemplatePtr> Node::getTemplate() const
{
	return {};
}

