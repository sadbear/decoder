#include <unordered_set>
#include <cstring>

#include "../Decoder.hpp"
#include "../ContextNode.hpp"
#include "../../Specification/Operand.hpp"

using namespace specification;

parser::Decoder::Decoder(std::vector<Instruction> instructions)
{
    _root = ContextNode::create();

    initializeTemplatesNode(initializeContextNodes(instructions));
}

std::map<Instruction, parser::Node::Ptr>
parser::Decoder::initializeContextNodes(const std::vector<Instruction> &instructions)
{
    std::map<Instruction, Node::Ptr> contexts;

    for (const auto & instr: instructions)
    {
        if (!instr._context.empty())
            contexts[instr] = _root->pushNode(ContextNode::create(instr._context));
        else
            contexts[instr] = _root;
    }
    return contexts;
}

void parser::Decoder::initializeTemplatesNode(const std::map<Instruction, Node::Ptr> & instructions)
{
    for (const auto & [instruction, context_node]: instructions)
    {
        auto ptr = context_node;

        auto operands = std::make_shared<OperandCollection>();

        for (const auto & _template: instruction._templates)
        {
            Node::Ptr templateNode = nullptr;
            if (_hashTable.find(_template) != _hashTable.end())
            {
                templateNode = _hashTable.at(_template);
            }
            else
            {
                if (auto morpheme = std::dynamic_pointer_cast<Morpheme>(_template); morpheme)
                {
                    templateNode = MorphemeNode::create(morpheme);
                }
                else if (auto operand = std::dynamic_pointer_cast<Operand>(_template); operand)
                {
                    operands->insert(*operand);
                    continue;
                }
                else
                    throw std::logic_error("Unknown type of template in instructions.");

                _hashTable[_template] = templateNode;
            }

            ptr = ptr->pushNode(templateNode);
        }

        if (operands->empty())
        {
            // ToDo Add full path check
            continue;
        }

        Node::Ptr operandNode = nullptr;
        try
        {
             operandNode = _hashTable[operands];
        }
        catch (std::out_of_range&)
        {
            operandNode = OperandNode::create(operands);
            _hashTable[operands] = operandNode;
        }
        if (operandNode)
            ptr->pushNode(operandNode);
    }
}

void parser::Decoder::find(specification::CheckContextFunc & contextCheck,
                           storage::ISignatureSource::Ptr & source,
                           parser::TemplateProcess & processor) const
{
    _root->find(contextCheck, source, processor);
}

void parser::Decoder::traversal(std::function<void(const parser::Node::Ptr&)> & callback) const
{
    callback(_root);
    _root->traversal(callback);
}

void parser::Decoder::compress()
{
    _root->compress();
}


