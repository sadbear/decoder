
#include "../OperandNode.hpp"


parser::OperandNode::OperandNode() = default;

parser::Result<parser::Node::Ptr> parser::OperandNode::find(const specification::CheckContextFunc &context,
                                                            storage::ISignatureSource::Ptr,
                                                            const parser::NodeProcessor &callback)
{
    return {R_TRUE, nullptr};
}

bool
parser::OperandNode::check(storage::ISignatureSource::Ptr signature, const specification::CheckContextFunc &context)
{
    signature->update(_operands->getIndexes());
    try
    {
        const auto result = std::get<bool>(_operands->check(signature->getSignature()));
        return result;
    }
    catch (std::bad_variant_access &)
    {
        return false;
    }
}

parser::Node::Ptr parser::OperandNode::pushNode(const Node::Ptr &node)
{
    throw std::logic_error("Can't push to operand node."); // ToDo
}

size_t parser::OperandNode::getWeight() const
{
    return _operands->getWeight();
}

bool parser::OperandNode::isFullPath() const
{
    return true;
}

bool parser::OperandNode::contains(parser::Node::Ptr other) const
{
    throw std::logic_error("Not implemented");
}

const parser::Node::Vector & parser::OperandNode::getChildren() const
{
    return _children;
}

void parser::OperandNode::compress() {}

std::optional<specification::TemplatePtr> parser::OperandNode::getTemplate() const
{
    return std::dynamic_pointer_cast<specification::ITemplate>(_operands);
}

void parser::OperandNode::appendChildren(const parser::Node::Ptr &child)
{
    throw std::logic_error("");
}

parser::Node::Ptr parser::OperandNode::create(const specification::OperandCollection::Ptr &collection)
{
    return std::dynamic_pointer_cast<Node>(std::make_shared<OperandNode>(collection));
}

parser::OperandNode::OperandNode(const specification::OperandCollection::Ptr &collection): _operands(collection)
{
}

