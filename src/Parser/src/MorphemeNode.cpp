#include <sstream>
#include "../MorphemeNode.hpp"

parser::MorphemeNode::MorphemeNode(const specification::Morpheme::Ptr &morpheme, bool isFullPath) :
		_morpheme(morpheme), _full(isFullPath)
{}

parser::Node::Ptr parser::MorphemeNode::create(const specification::Morpheme::Ptr &morpheme, bool isFullPath)
{
	return std::dynamic_pointer_cast<parser::Node>(std::make_shared<parser::MorphemeNode>(morpheme, isFullPath));
}

parser::Result<parser::Node::Ptr> parser::MorphemeNode::find(
		const specification::CheckContextFunc &context,
		storage::ISignatureSource::Ptr signature_source,
		const NodeProcessor &callback
)
{
	signature_source->update(getMorpheme()->getIndexes());
	if (check(signature_source->getSignature() ^ getMorpheme()->getIndexes()))
	{
		if (isFullPath())
			return {R_TRUE, nullptr};
	}
	else
	{
		return {R_FALSE, nullptr};
	}

	for (const auto & child: getMorphemeNodeChildren())
	{
		signature_source->update(child->getMorpheme()->getIndexes());
		const auto signature = signature_source->getSignature();
		
		if (child->contains(signature))
		{
			auto result = child->find(context, signature_source, callback);
			switch (result.first)
			{
				case R_TRUE:
					if (result.second == nullptr)
						result.second = child;
					
					callback(_morpheme);
					return result;
				
				case R_MAYBE:
					// ToDo if search fails -> add not rejected paths
					continue;
				
				case R_FALSE:
					continue;
			}
		}
	}
	
	return {R_MAYBE, nullptr};
}

bool parser::MorphemeNode::check(
		storage::ISignatureSource::Ptr signature_source, const specification::CheckContextFunc &context
)
{
	signature_source->update(_morpheme->getIndexes());
	return check(signature_source->getSignature(_morpheme->getIndexes()));
}

bool parser::MorphemeNode::check(const specification::Signature &signature)
{
	const auto result = _morpheme->check(signature);
	try
	{
		return std::get<bool>(result);
	}
	catch (std::bad_variant_access &)
	{
		return false;
	}
}

size_t parser::MorphemeNode::getWeight() const
{
	return _morpheme->size();
}

bool parser::MorphemeNode::isFullPath() const
{
	return _full;
}

parser::Node::Ptr parser::MorphemeNode::pushNode(const Node::Ptr &node_)
{
	if (!node_->getChildren().empty())
		throw std::logic_error("Can't push node with children.");
	
	if (auto newNode = std::dynamic_pointer_cast<MorphemeNode>(node_); newNode)
	{
		if (const auto signature =
					_morpheme->getSignature() ^newNode->getMorpheme()->getSignature(); !signature.empty())
		{
			if (signature.size() == newNode->getMorpheme()->getSignature().size())
			{
				if (_morpheme->isConstant() && newNode->getMorpheme()->isConstant())
					return nullptr; // Current node is full contain inserting signature
				
				std::stringstream error("Ambiguous morpheme error for different instructions");
				if (auto morpheme = _morpheme->toString(); morpheme)
					error << " " << *morpheme << ";";
				if (auto morpheme = newNode->getMorpheme()->toString(); morpheme)
					error << " " << *morpheme << ";";
				throw std::logic_error(error.str());
			} else if (signature.size() == _morpheme->getSignature().size())
			{
				newNode->crop(signature.getIndexes());
			} else
			{
				auto pulled = pullNode(signature);
				newNode->crop(signature.getIndexes());
				appendChildren(node_);
				return node_;
			}
		}
	}
	for (auto &child_: _children)
	{
		if (auto child = std::dynamic_pointer_cast<MorphemeNode>(child_); child)
		{
			if (child->contains(node_))
			{
				auto result = child->pushNode(node_);
				return result == nullptr ? child_ : result;
			}
		}
	}
	appendChildren(node_);
	return node_;
}

const specification::Morpheme::Ptr &parser::MorphemeNode::getMorpheme() const
{
	return _morpheme;
}

void parser::MorphemeNode::crop(const specification::Indexes &indexes)
{
	_morpheme->operator-=(indexes);
}

parser::Node::Ptr parser::MorphemeNode::pullNode(specification::Signature signature)
{
	signature = signature ^ _morpheme->getSignature();
	
	specification::Morpheme::Ptr current = _morpheme;
	_morpheme = std::make_shared<specification::Morpheme>(signature);
	current->operator-=(signature.getIndexes());
	
	auto node = MorphemeNode::create(current);
	node->_children = _children;
	_children = Vector();
	
	appendChildren(node);
	return node;
}

bool parser::MorphemeNode::contains(parser::Node::Ptr other) const
{
	if (auto node = std::dynamic_pointer_cast<MorphemeNode>(other); other)
		return contains(node->getMorpheme()->getSignature());
	return false;
}

bool parser::MorphemeNode::contains(const specification::Signature &other) const
{
	return !(_morpheme->getSignature() ^ other).empty();
}

std::optional<specification::TemplatePtr> parser::MorphemeNode::getTemplate() const
{
	return _morpheme;
}

void parser::MorphemeNode::compress()
{
	while (getChildren().size() == 1)
		if (auto morphemeChild = std::dynamic_pointer_cast<MorphemeNode>(getChildren()[0]);
				morphemeChild && (getMorpheme()->isConstant() || morphemeChild->getMorpheme()->isConstant()))
		{
			_morpheme = _morpheme + morphemeChild->getMorpheme();
			_children = morphemeChild->_children;
			
			morphemeChild->_children.clear();
		} else
		{
			break;
		}
	
	std::for_each(_children.begin(), _children.end(), [](Node::Ptr child)
	{
		child->compress();
	});
	
}

std::vector<parser::MorphemeNode::Ptr> parser::MorphemeNode::getMorphemeNodeChildren() const
{
	std::vector<MorphemeNode::Ptr> result;
	for (const auto & child_: getChildren())
	{
		if (auto child = std::dynamic_pointer_cast<MorphemeNode>(child_); child_)
		{
			result.push_back(child);
		}
	}
	return result;
}

