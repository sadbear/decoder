#ifndef DECODER_SPECIFICATION_SOURCE_HPP
#define DECODER_SPECIFICATION_SOURCE_HPP

#include "../Specification/Instruction.hpp"

namespace storage
{
    class ISpecificationSource
    {
    public:
        virtual specification::Instruction next() = 0;
        virtual bool isEnd() const = 0;
    };
}

#endif //DECODER_SPECIFICATION_SOURCE_HPP
