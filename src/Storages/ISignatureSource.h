#ifndef DECODER_SOURCE_STREAM_H
#define DECODER_SOURCE_STREAM_H

#include <vector>
#include <memory>
#include "../Specification/Indexes.hpp"
#include "../Specification/Signature.hpp"

namespace storage
{
	using BitVector = std::vector<bool>;

	struct ISignatureSource
	{
		using Ptr = std::shared_ptr<ISignatureSource>;

		virtual bool update(const specification::Indexes &length) = 0;

		virtual size_t current_size() const = 0;

		virtual specification::Indexes getIndexes() const = 0;

		virtual specification::Signature getSignature(const specification::Indexes &indexes) const = 0;

		virtual specification::Signature getSignature() const = 0;

		virtual bool isSourceEnd() const = 0;
	};
}

#endif //DECODER_SOURCE_STREAM_H
