#ifndef DECODER_BLANK_STREAM_H
#define DECODER_BLANK_STREAM_H

#include "ISignatureSource.h"
#include "../Specification/Signature.hpp"

namespace storage
{
    class BlankStorage: public ISignatureSource
    {
    public:
    	static Ptr create(std::string_view bits);
	    static Ptr create(uint64_t bits, size_t size = 64);

    public:
        explicit BlankStorage(std::string_view bits);
        explicit BlankStorage(const BitVector & bits);

	    BlankStorage(std::initializer_list<bool> bits);

        bool update(const specification::Indexes &indexes) override;
        size_t current_size() const override;

	    specification::Indexes getIndexes() const override;

	    specification::Signature getSignature() const override;
	    specification::Signature getSignature(const specification::Indexes &indexes) const override;

	    bool isSourceEnd() const override;

    private:
        BitVector::iterator     _pointer;
        BitVector               _vector;
        specification::Indexes  _indexes;
    };
}

#endif //DECODER_BLANK_STREAM_H
