#include "../BlankStorage.h"

using namespace storage;

bool BlankStorage::update(const specification::Indexes &indexes)
{
    _indexes = _indexes + indexes;
    return true;
}

BlankStorage::BlankStorage(const BitVector &bits) :_vector(bits)
{
    _pointer = _vector.begin();
}

size_t BlankStorage::current_size() const
{
    return static_cast<size_t>(std::distance(_vector.begin(), BitVector::const_iterator(_pointer)));
}

BlankStorage::BlankStorage(std::string_view bits)
{
    _vector.reserve(bits.size());

    for (const char & chr: bits)
        _vector.push_back(chr == '1');

    _pointer = _vector.begin();
}

bool BlankStorage::isSourceEnd() const
{
    return _pointer == _vector.end();
}

BlankStorage::BlankStorage(std::initializer_list<bool> values) : _vector(values)
{
	_pointer = _vector.begin();
}

ISignatureSource::Ptr BlankStorage::create(std::string_view bits)
{
	auto ptr = std::make_shared<BlankStorage>(bits);
	return std::dynamic_pointer_cast<ISignatureSource>(ptr);
}

ISignatureSource::Ptr BlankStorage::create(uint64_t bits, size_t size)
{
	BitVector result;
	while (--size != 0)
		result.push_back(bool((bits >> size) & 1u));


	auto storage = std::make_shared<BlankStorage>(result);
	return std::dynamic_pointer_cast<ISignatureSource>(storage);
}

specification::Indexes BlankStorage::getIndexes() const
{
	return _indexes;
}

specification::Signature BlankStorage::getSignature() const
{
	if (_indexes.empty())
		return specification::Signature();

    const auto indexes = specification::Indexes::getRange(0, _indexes.back() + 1);
    const auto constants = std::vector<bool>(_vector.begin(), _vector.begin() + indexes.size());
	return specification::Signature{indexes, constants};
}

specification::Signature BlankStorage::getSignature(const specification::Indexes &indexes) const
{
	return getSignature() ^ indexes;
}

