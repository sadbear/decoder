#ifndef DECODER_HASH_COMBINE_HPP
#define DECODER_HASH_COMBINE_HPP

#include <cstdio>
#include <functional>

template<typename T>
void _hash_combine(size_t &seed, const T &val)
{
    seed ^= std::hash<T>()(val) + 0x9e3779b9 + size_t(seed << 6) + (seed >> 2);
}

template<typename... Types>
size_t hash_combine(const Types &... args)
{
    size_t seed = 0;
    (_hash_combine(seed, args), ... ); // create hash value with seed over all args
    return seed;
}

#endif //DECODER_HASH_COMBINE_HPP
