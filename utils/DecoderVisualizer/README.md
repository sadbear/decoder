# Requirements
Python3
# Instruction
## Windows:
1. `pip install pydot`
2. Download graphviz from https://graphviz.gitlab.io/download/
3. Add `C:\Program Files (x86)\Graphviz2.38\bin` to `PATH`

## Arch Linux
1. `pip install pydot Image`
2. `sudo pacman -S graphviz`

# Usage
`python treeviz.py graph.json [output_image.png]`

# Required json format

```
...
{
    "hash": 4,
    "type": "morpheme",
    "indexes": [0,1,2,3],
    "constant": "0xf",
    "mnemononic": ["ADD", "Affix"],
    "children": [5, 6]
},
{
    "hash": 5,
    "type": "morpheme",
    "indexes": [4,5,6,7],
    "constant": "0xe",
    "children": []
},
{
    "hash": 6,
    "indexes": [8,9,10,11],
    "type": "operands",
    "operands": ["REGISTER", "IMMEDIATE"],
    "children": []
}
...
```

# Example of image
![Example](example/example.json.png)
