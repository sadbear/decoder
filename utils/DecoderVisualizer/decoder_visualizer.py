#!/bin/python3

import json
from pathlib import Path

import pydot
import sys

from typing import Dict, Tuple

_order = ("id", "type", "indexes"), ("children",)


def _correct_order(val: Dict[str, str]) -> tuple:
    assert all(field in val for field in (*_order[0], *_order[1])), \
        "Check json file. Can't find required fields."

    head = tuple((key, val.pop(key)) for key in _order[0])
    tuple((key, val.pop(key)) for key in _order[1])
    tail = tuple()

    return (*head, *val.items(), *tail)


def _get_label(node: dict) -> str:
    return ''.join(f'{key.title()}: {value}\l' for key, value in _correct_order(node) if value or value == 0)


def _plot_graph(graph_filename: Path, image_filename: Path):
    with open(str(graph_filename)) as fp:
        nodes: Tuple[Dict[str, str]] = json.load(fp)

    graph = pydot.Dot(graph_type='digraph') #, rankdir='LR')

    edges = {}
    for node in nodes:
        if node['children']:
            edges[node['id']] = node['children']

        graph.add_node(pydot.Node(name=str(node['id']),
                                  label=_get_label(node),
                                  shape='box'))

    for hash_, children in edges.items():
        for child in children:
            graph.add_edge(pydot.Edge(str(hash_), str(child)))

    graph.write_png(str(image_filename))


def plot_graph(input_filename: Path, image_filename: Path = None, is_open: bool = False):

    image_filename = image_filename or (Path.cwd() / f"{input_filename}.png")

    _plot_graph(input_filename, image_filename)

    if not is_open:
        return

    import os
    if os.name == 'nt':  # Windows
        os.startfile(str(image_filename))
    else:
        import subprocess
        subprocess.run(["feh", str(image_filename)])


if __name__ == '__main__':
    sys.tracebacklimit = 0
    assert len(sys.argv) >= 2, f"Usage: {sys.argv[0]} graph.json [output_image.png]"

    input_file: Path = Path(sys.argv[1])
    if input_file.is_dir():
        for file in filter(lambda fp: fp.suffix == ".json", input_file.iterdir()):
            image_file: Path = Path(sys.argv[2]) if len(sys.argv) >= 3 else Path.cwd() / f"{file}.png"

            if image_file.exists():
                print(f"Overwrite file {image_file}")

            plot_graph(file, image_file, False)

    else:
        assert input_file.exists(), "File not exists"
        assert input_file.suffix == ".json", "Wrong file type. Please, use json."

        image_file: Path = Path(sys.argv[2]) if len(sys.argv) >= 3 else Path.cwd() / f"{input_file}.png"
        if image_file.exists():
            is_overwrite = input(f"File {str(image_file)} exist. Overwrite? [Y/n]")
            if is_overwrite.lower() not in ('', 'y', 'yes'):
                exit()

        plot_graph(input_file, image_file, False)
