#!/bin/python3

import sys
from pathlib import Path
from time import sleep

from decoder_visualizer import plot_graph

if __name__ == '__main__':
    input_file: Path = Path(sys.argv[1])
    assert input_file.exists()
    assert input_file.is_dir()

    while True:
        for file in filter(lambda f: f.suffix == ".json", input_file.iterdir()):
            if file.exists():
                plot_graph(file)
                print(file.name)

        sleep(5)
